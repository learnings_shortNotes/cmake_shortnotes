Bitbake
============

## Variables

### Set Variable values
```
VARIABLE = "value"

// variable with leading or trailing spaces are retained
VARIABLE = " value"
VARIABLE = "value "
```

### Expanding variable values
```
A = "and"
B = "Exp${A}ing"
```
>Here the value for B => Expanding

### Setting Default value
To set the default value to a varible if it is undefined.

#### Soft variable assignment
```
A ?= "something_default_val"
```

#### Weak variable assignment
```
A ??= "Assigned_at_end_of_parsing"
```
No difference with the above one except that the assignment is made at the end of the parsing process rather than immediately

>Note: If multiple assignments for the same variable, the first assignment will be taken.

### Immediate Variable Expansion
Here the variable will be used immediately rather than where
the variable is actually used.
```
T = "car"
A := "${B} ${A} driving a ${T}"
B = "Am"
```
>The value of A => driving a car.

### Appending(+=) and prependin(=+) variable with space
To append/prepend a variable with spaces
```
A = "This is"
A =+ "a car"
C = "am i doing"
C += "What"
```
> A => This is a car ; C => What am i doing

### Appending(.=) and prependin(=.) variable without space
To append/prepend a variable without spaces
```
A = "This is"
A =. "a car"
C = "am i doing"
C .= "What"
```
> A => This isa car ;  C => Whatam i doing

### Appending and Prepending (Override Style Syntax)
Here no spaces are inserted. The operators "_append" and
"_prepend" differ from the operators "*.=*" and "*=.*" in
that they are deferred until after parsing completes rather than
being immediately applied. 
```
 B = "bval"
 B_append = "_additional data"
 C = "cval"
 C_prepend = "additional data_"
```
>B => bval_additional data ;   C => additional data_cval

### Removal (Override Style Syntax)
Operator: *_remove*
```
FOO = "123 456 789 123456 123 456 123 456"
FOO_remove = "123"
FOO_remove = "456"
FOO2 = "abc def ghi abcdef abc def abc def"
FOO2_remove = "abc def"
```
>FOO => 789 123456 ; FOO2 => ghi abcdef

### Variable Flag Syntax
Tag extra information on a variable. 
```
FOO[a] = "something_alpha"
FOO[b] = "123456789"
```
>FOO has two flags: a => something_alpha ; b = >123456789

### Inline Python Variable Expansion
You can use inline Python variable expansion to set
variables.
```
DATE = "${@time.strftime('%Y%m%d',time.gmtime())}"
```
> Date => current time

### Varaible to store pathname
`Do not add "~" in your path directory.`
```
BBLAYERS ?= " \
       /home/scott-lenovo/LayerA \
       "
```

## OVERRIDES (condtional syntax)
BitBake uses OVERRIDES to control what variables are
overridden after BitBake parses recipes and configuration 
files.

### Conditional Metadata
- **Selecting a variable**: 

    To set your override, use the OVERIDES variable. It is a colon-character-seperated list that contains for which
    you want to satisfy conditions.

    ```
    OVERRIDES = "architecture:os:machine"
    TEST = "default"
    TEST_os = "osspecific"
    TEST_nooverride = "othercondvalue"
    ```
    > Test => default ; Test_os = osspecific
    
