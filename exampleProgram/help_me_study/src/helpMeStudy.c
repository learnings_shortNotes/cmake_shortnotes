#include <stdio.h>
#include <stdlib.h>
#include "../include/nodeSaver.h"

#define errorCode "\033[41m"  
#define endColourCoding "\033[0m"
#define warningCode "\033[0;33m"

void printStrin(char strin[], int lm)
{
    int k=0;
    while((strin[k]!= '\0') && (k<=lm))
        {
            printf("%c",strin[k]);
            k++;
        }
}
void read_data_from_file()
{
    char data[100][250],ch;
    int lines = 0, strins = 0; 
    FILE *inputFile = fopen("/home/vaisakh/work/cScripts/help_me_study/sample_files/bitbake_shortnotes/README.md", "r");
    if (inputFile == NULL)
    {
        printf("Couldn't Read file.");
        exit (0);        
    }
    
    printf("Data from the file:\n");

    while((ch = getc(inputFile))!= EOF)
    {
        if (lines >= 100)
            {
                printf("\nMaximum number of lines reached..%s[Program Abort]%s\n\n", errorCode, endColourCoding);
                exit (0);
            }
        if ( strins >= 249)
            {
                printf(" [per array max readched] %s(injected '/0' )%s",warningCode, endColourCoding);
                data[lines][249] = '\0';
            }    
        if(ch != '\n')
            {
                data[lines][strins++] = ch;
            } 
        else
        {
            data[lines++][strins] = '\0';
            strins = 0;
            printStrin(data[lines-1],250);
            getchar();
        }    

    }
    fclose(inputFile);
    data[lines++][strins] = '\0';
}

int main (int argc , char*   argv[])
{
    simple_function();
    read_data_from_file();
}