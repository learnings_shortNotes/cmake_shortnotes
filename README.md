CMAKE
============
>*Tutorial from [here](http://derekmolloy.ie/hello-world-introductions-to-cmake)*

Should have file : **CMakeLists.txt** in directory.

The CMakeLists.txt should contain:

```
cmake_minimum_required(VERSION 2.8.9)
project (hello)
add_executable(hello helloworld.cpp)
```

## The Multi-directory ProjecT
For  multiple project to be added use the following statements.
```
project(directory_test) 
include_directories(include)
file(GLOB SOURCES "src/*.cpp")
#OR
    #set(SOURCES src/mainapp.cpp src/Student.cpp)
message("Reieved files: ${SOURCES}")
add_executable(testStudent ${SOURCES})
```